package ru.zolov.tm.endpoint;

import java.text.ParseException;
import java.util.Comparator;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.ITaskEndpoint;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Session;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.exception.AccessForbiddenException;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;


@NoArgsConstructor
@WebService(endpointInterface = "ru.zolov.tm.api.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

  @SneakyThrows @NotNull @Override @WebMethod public Task addNewTask(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "name") final String name,
      @NotNull @WebParam(name = "description") final String description,
      @NotNull @WebParam(name = "start") final String start,
      @NotNull @WebParam(name = "finish") final String finish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().create(session.getUserId(), projectId, name, description, start, finish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> findAllTasksByUserId(
      @NotNull @WebParam(name = "session") final Session session
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().readAll(session.getUserId());
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> findTasksByProjectId(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().readAll(session.getUserId());
  }

  @SneakyThrows @Nullable @Override @WebMethod public Task findOneTaskById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().readTaskById(taskId);
  }

  @SneakyThrows @Override @WebMethod public void removeOneTaskById(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "taskId") final String taskId
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getTaskService().remove(session.getUserId(), taskId);
  }

  @SneakyThrows @Override @WebMethod public void updateTask(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "taskId") final String taskId,
      @NotNull @WebParam(name = "taskName") final String taskName,
      @NotNull @WebParam(name = "taskDescription") final String taskDescription,
      @NotNull @WebParam(name = "dateOfstart") final String dateOfstart,
      @NotNull @WebParam(name = "dateOffinish") final String dateOffinish
  ) throws AccessForbiddenException, CloneNotSupportedException, ParseException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    serviceLocator.getTaskService().update(session.getUserId(), taskId, taskName, taskDescription, dateOfstart, dateOffinish);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> getSortedTaskList(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "projectId") final String projectId,
      @NotNull @WebParam(name = "comparatorName") final String comparatorName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyStringException, EmptyRepositoryException {
    serviceLocator.getSessionService().validate(session);
    final Comparator<AbstractGoal> comparator = serviceLocator.getTerminalService().getComparator(comparatorName);
    return serviceLocator.getTaskService().sortBy(session.getUserId(), projectId, comparator);
  }

  @SneakyThrows @NotNull @Override @WebMethod public List<Task> findTaskByPartOfTheName(
      @NotNull @WebParam(name = "session") final Session session,
      @NotNull @WebParam(name = "partOfTheName") final String partOfTheName
  ) throws AccessForbiddenException, CloneNotSupportedException, EmptyRepositoryException, EmptyStringException {
    serviceLocator.getSessionService().validate(session);
    return serviceLocator.getTaskService().findTask(session.getUserId(), partOfTheName);
  }

}
