package ru.zolov.tm.endpoint;


import ru.zolov.tm.api.ServiceLocator;

public abstract class AbstractEndpoint {

  protected ServiceLocator serviceLocator;

  public void setServiceLocator(ServiceLocator serviceLocator) {
    this.serviceLocator = serviceLocator;
  }
}
