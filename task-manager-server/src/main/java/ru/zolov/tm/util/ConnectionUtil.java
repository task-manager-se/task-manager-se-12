package ru.zolov.tm.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.exception.EmptyStringException;

public class ConnectionUtil {

  public static Connection getConnection() {
    @NotNull final Properties connectionProperties = new Properties();
    @Nullable FileInputStream fis;
    @Nullable Connection connection = null;

    try {
      fis = new FileInputStream("task-manager-server/src/main/resources/dbconnection.properties");
      connectionProperties.load(fis);
      @Nullable String url = connectionProperties.getProperty("url");
      @Nullable String user = connectionProperties.getProperty("login");
      @Nullable String password = connectionProperties.getProperty("password");
      if (url == null) throw new EmptyStringException();
      connection = DriverManager.getConnection(url, user, password);
      if (connection != null) System.out.println("Connected!");
    } catch (EmptyStringException | SQLException | IOException e) {
      e.printStackTrace();
    }
  return connection;
  }
}
