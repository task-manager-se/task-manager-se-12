package ru.zolov.tm.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.api.IProjectService;
import ru.zolov.tm.entity.AbstractGoal;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.util.ConnectionUtil;

public class ProjectService extends AbstractService<Project> implements IProjectService {

  @SneakyThrows @NotNull public Project create(
      @Nullable final String userId,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable String start,
      @Nullable String finish
  ) throws EmptyStringException, ParseException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (name == null || name.isEmpty()) throw new EmptyStringException();
    if (description == null || description.isEmpty()) throw new EmptyStringException();
    if (start == null || start.isEmpty()) throw new EmptyStringException();
    if (finish == null || finish.isEmpty()) throw new EmptyStringException();
    final Project project = new Project();
    project.setName(name);
    project.setUserId(userId);
    project.setDescription(description);
    project.setDateOfStart(dateFormat.parse(start));
    project.setDateOfFinish(dateFormat.parse(finish));
    projectRepository.persist(project);
    return project;
  }

  @NotNull public Project read(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, EmptyRepositoryException, SQLException {
    ProjectRepository projectRepository = new ProjectRepository(ConnectionUtil.getConnection());
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    if (projectRepository.findOne(id) == null) throw new EmptyRepositoryException();
    return projectRepository.findOne(id);
  }

  @NotNull public List<Project> readAll(@Nullable final String userId) throws EmptyStringException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    if (userId == null) throw new EmptyStringException();
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAllByUserId(userId);

    return list;
  }

  @NotNull public List<Project> readAll() throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAll();
    return list;
  }

  public void update(
      @Nullable final String userId,
      @Nullable final String id,
      @Nullable final String name,
      @Nullable final String description,
      @Nullable String start,
      @Nullable String finish
  ) throws ParseException, EmptyStringException, EmptyRepositoryException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    if (userId == null || userId.isEmpty()) return;
    if (id == null || id.isEmpty()) return;
    if (name == null || name.isEmpty()) return;
    @NotNull final Project project = new Project();
    project.setUserId(userId);
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    final Date startDate = dateFormat.parse(start);
    project.setDateOfStart(startDate);
    final Date finishDate = dateFormat.parse(finish);
    project.setDateOfFinish(finishDate);
    projectRepository.merge(project);

  }

  public void remove(
      @Nullable final String userId,
      @Nullable final String id
  ) throws EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (id == null || id.isEmpty()) throw new EmptyStringException();
    projectRepository.remove(id);
  }

  @NotNull public List<Project> sortBy(
      @Nullable final String userId,
      Comparator<AbstractGoal> comparator
  ) throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    @NotNull List<Project> list = new ArrayList<>();
    list = projectRepository.findAll();
    Collections.sort(list, comparator);
    return list;
  }

  @NotNull public List<Project> findProject(
      @Nullable final String userId,
      @Nullable final String partOfTheName
  ) throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    if (userId == null || userId.isEmpty()) throw new EmptyStringException();
    if (partOfTheName == null) throw new EmptyStringException();
    @NotNull List<Project> defaultlist = new ArrayList<>();
    @NotNull List<Project> list = new ArrayList<>();
    defaultlist = projectRepository.findAll();
    list = projectRepository.findProjectByPartOfTheName(userId, partOfTheName);
    if (partOfTheName.isEmpty()) return defaultlist;
    return list;
  }

  public void load(@Nullable List<Project> list) throws EmptyRepositoryException, EmptyStringException, SQLException {
    @Cleanup @Nullable Connection connection = ConnectionUtil.getConnection();
    @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
    if (list == null) throw new EmptyRepositoryException();
    projectRepository.load(list);
  }
}
