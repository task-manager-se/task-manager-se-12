package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.exception.EmptyStringException;

public interface IProjectRepository extends IRepository<Project> {

  @NotNull List<Project> findProjectByPartOfTheName(
      @NotNull String userId,
      @NotNull String partOfTheName
  ) throws SQLException, EmptyStringException;

  @Nullable Project findOneByUserId(
      @NotNull String userId,
      @NotNull String id
  ) throws SQLException, EmptyStringException;

  @NotNull List<Project> findAllByUserId(@NotNull String userId) throws SQLException, EmptyStringException;

  void removeAllByUserId(@NotNull String userId) throws SQLException;
}
