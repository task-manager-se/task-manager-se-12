package ru.zolov.tm.api;

import java.sql.SQLException;
import java.util.List;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zolov.tm.entity.AbstractEntity;
import ru.zolov.tm.exception.EmptyRepositoryException;
import ru.zolov.tm.exception.EmptyStringException;

public interface IRepository<E extends AbstractEntity> {

  @NotNull List<E> findAll() throws EmptyRepositoryException, SQLException, EmptyStringException;

  @Nullable E findOne(String id) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void persist(@NotNull E entity) throws SQLException, EmptyStringException;

  void merge(@NotNull E entity) throws EmptyStringException, EmptyRepositoryException, SQLException;

  void remove(@NotNull String id) throws SQLException;

  void removeAll() throws SQLException;

  void load(@NotNull List<E> list) throws SQLException, EmptyStringException, EmptyRepositoryException;
}
