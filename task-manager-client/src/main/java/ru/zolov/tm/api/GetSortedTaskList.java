
package ru.zolov.tm.api;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getSortedTaskList complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="getSortedTaskList"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="session" type="{http://api.tm.zolov.ru/}session" minOccurs="0"/&gt;
 *         &lt;element name="projectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="comparatorName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getSortedTaskList", propOrder = {"session", "projectId", "comparatorName"})
public class GetSortedTaskList {

  protected Session session;
  protected String projectId;
  protected String comparatorName;

  /**
   * Gets the value of the session property.
   *
   * @return
   *     possible object is
   *     {@link Session }
   *
   */
  public Session getSession() {
    return session;
  }

  /**
   * Sets the value of the session property.
   *
   * @param value
   *     allowed object is
   *     {@link Session }
   *
   */
  public void setSession(Session value) {
    this.session = value;
  }

  /**
   * Gets the value of the projectId property.
   *
   * @return
   *     possible object is
   *     {@link String }
   *
   */
  public String getProjectId() {
    return projectId;
  }

  /**
   * Sets the value of the projectId property.
   *
   * @param value
   *     allowed object is
   *     {@link String }
   *
   */
  public void setProjectId(String value) {
    this.projectId = value;
  }

  /**
   * Gets the value of the comparatorName property.
   *
   * @return
   *     possible object is
   *     {@link String }
   *
   */
  public String getComparatorName() {
    return comparatorName;
  }

  /**
   * Sets the value of the comparatorName property.
   *
   * @param value
   *     allowed object is
   *     {@link String }
   *
   */
  public void setComparatorName(String value) {
    this.comparatorName = value;
  }

}
