
package ru.zolov.tm.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.zolov.tm.api package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

  private final static QName _AccessForbiddenException_QNAME = new QName("http://api.tm.zolov.ru/", "AccessForbiddenException");
  private final static QName _ClassNotFoundException_QNAME = new QName("http://api.tm.zolov.ru/", "ClassNotFoundException");
  private final static QName _CloneNotSupportedException_QNAME = new QName("http://api.tm.zolov.ru/", "CloneNotSupportedException");
  private final static QName _EmptyRepositoryException_QNAME = new QName("http://api.tm.zolov.ru/", "EmptyRepositoryException");
  private final static QName _EmptyStringException_QNAME = new QName("http://api.tm.zolov.ru/", "EmptyStringException");
  private final static QName _IOException_QNAME = new QName("http://api.tm.zolov.ru/", "IOException");
  private final static QName _JAXBException_QNAME = new QName("http://api.tm.zolov.ru/", "JAXBException");
  private final static QName _SQLException_QNAME = new QName("http://api.tm.zolov.ru/", "SQLException");
  private final static QName _LoadFromBin_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromBin");
  private final static QName _LoadFromBinResponse_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromBinResponse");
  private final static QName _LoadFromJsonJackson_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromJsonJackson");
  private final static QName _LoadFromJsonJacksonResponse_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromJsonJacksonResponse");
  private final static QName _LoadFromJsonJaxb_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromJsonJaxb");
  private final static QName _LoadFromJsonJaxbResponse_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromJsonJaxbResponse");
  private final static QName _LoadFromXmlJackson_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromXmlJackson");
  private final static QName _LoadFromXmlJacksonResponse_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromXmlJacksonResponse");
  private final static QName _LoadFromXmlJaxb_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromXmlJaxb");
  private final static QName _LoadFromXmlJaxbResponse_QNAME = new QName("http://api.tm.zolov.ru/", "loadFromXmlJaxbResponse");
  private final static QName _SaveToBin_QNAME = new QName("http://api.tm.zolov.ru/", "saveToBin");
  private final static QName _SaveToBinResponse_QNAME = new QName("http://api.tm.zolov.ru/", "saveToBinResponse");
  private final static QName _SaveToJsonJackson_QNAME = new QName("http://api.tm.zolov.ru/", "saveToJsonJackson");
  private final static QName _SaveToJsonJacksonResponse_QNAME = new QName("http://api.tm.zolov.ru/", "saveToJsonJacksonResponse");
  private final static QName _SaveToJsonJaxb_QNAME = new QName("http://api.tm.zolov.ru/", "saveToJsonJaxb");
  private final static QName _SaveToJsonJaxbResponse_QNAME = new QName("http://api.tm.zolov.ru/", "saveToJsonJaxbResponse");
  private final static QName _SaveToXmlJackson_QNAME = new QName("http://api.tm.zolov.ru/", "saveToXmlJackson");
  private final static QName _SaveToXmlJacksonResponse_QNAME = new QName("http://api.tm.zolov.ru/", "saveToXmlJacksonResponse");
  private final static QName _SaveToXmlJaxb_QNAME = new QName("http://api.tm.zolov.ru/", "saveToXmlJaxb");
  private final static QName _SaveToXmlJaxbResponse_QNAME = new QName("http://api.tm.zolov.ru/", "saveToXmlJaxbResponse");

  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.zolov.tm.api
   *
   */
  public ObjectFactory() {
  }

  /**
   * Create an instance of {@link AccessForbiddenException }
   *
   */
  public AccessForbiddenException createAccessForbiddenException() {
    return new AccessForbiddenException();
  }

  /**
   * Create an instance of {@link ClassNotFoundException }
   *
   */
  public ClassNotFoundException createClassNotFoundException() {
    return new ClassNotFoundException();
  }

  /**
   * Create an instance of {@link CloneNotSupportedException }
   *
   */
  public CloneNotSupportedException createCloneNotSupportedException() {
    return new CloneNotSupportedException();
  }

  /**
   * Create an instance of {@link EmptyRepositoryException }
   *
   */
  public EmptyRepositoryException createEmptyRepositoryException() {
    return new EmptyRepositoryException();
  }

  /**
   * Create an instance of {@link EmptyStringException }
   *
   */
  public EmptyStringException createEmptyStringException() {
    return new EmptyStringException();
  }

  /**
   * Create an instance of {@link IOException }
   *
   */
  public IOException createIOException() {
    return new IOException();
  }

  /**
   * Create an instance of {@link JAXBException }
   *
   */
  public JAXBException createJAXBException() {
    return new JAXBException();
  }

  /**
   * Create an instance of {@link SQLException }
   *
   */
  public SQLException createSQLException() {
    return new SQLException();
  }

  /**
   * Create an instance of {@link LoadFromBin }
   *
   */
  public LoadFromBin createLoadFromBin() {
    return new LoadFromBin();
  }

  /**
   * Create an instance of {@link LoadFromBinResponse }
   *
   */
  public LoadFromBinResponse createLoadFromBinResponse() {
    return new LoadFromBinResponse();
  }

  /**
   * Create an instance of {@link LoadFromJsonJackson }
   *
   */
  public LoadFromJsonJackson createLoadFromJsonJackson() {
    return new LoadFromJsonJackson();
  }

  /**
   * Create an instance of {@link LoadFromJsonJacksonResponse }
   *
   */
  public LoadFromJsonJacksonResponse createLoadFromJsonJacksonResponse() {
    return new LoadFromJsonJacksonResponse();
  }

  /**
   * Create an instance of {@link LoadFromJsonJaxb }
   *
   */
  public LoadFromJsonJaxb createLoadFromJsonJaxb() {
    return new LoadFromJsonJaxb();
  }

  /**
   * Create an instance of {@link LoadFromJsonJaxbResponse }
   *
   */
  public LoadFromJsonJaxbResponse createLoadFromJsonJaxbResponse() {
    return new LoadFromJsonJaxbResponse();
  }

  /**
   * Create an instance of {@link LoadFromXmlJackson }
   *
   */
  public LoadFromXmlJackson createLoadFromXmlJackson() {
    return new LoadFromXmlJackson();
  }

  /**
   * Create an instance of {@link LoadFromXmlJacksonResponse }
   *
   */
  public LoadFromXmlJacksonResponse createLoadFromXmlJacksonResponse() {
    return new LoadFromXmlJacksonResponse();
  }

  /**
   * Create an instance of {@link LoadFromXmlJaxb }
   *
   */
  public LoadFromXmlJaxb createLoadFromXmlJaxb() {
    return new LoadFromXmlJaxb();
  }

  /**
   * Create an instance of {@link LoadFromXmlJaxbResponse }
   *
   */
  public LoadFromXmlJaxbResponse createLoadFromXmlJaxbResponse() {
    return new LoadFromXmlJaxbResponse();
  }

  /**
   * Create an instance of {@link SaveToBin }
   *
   */
  public SaveToBin createSaveToBin() {
    return new SaveToBin();
  }

  /**
   * Create an instance of {@link SaveToBinResponse }
   *
   */
  public SaveToBinResponse createSaveToBinResponse() {
    return new SaveToBinResponse();
  }

  /**
   * Create an instance of {@link SaveToJsonJackson }
   *
   */
  public SaveToJsonJackson createSaveToJsonJackson() {
    return new SaveToJsonJackson();
  }

  /**
   * Create an instance of {@link SaveToJsonJacksonResponse }
   *
   */
  public SaveToJsonJacksonResponse createSaveToJsonJacksonResponse() {
    return new SaveToJsonJacksonResponse();
  }

  /**
   * Create an instance of {@link SaveToJsonJaxb }
   *
   */
  public SaveToJsonJaxb createSaveToJsonJaxb() {
    return new SaveToJsonJaxb();
  }

  /**
   * Create an instance of {@link SaveToJsonJaxbResponse }
   *
   */
  public SaveToJsonJaxbResponse createSaveToJsonJaxbResponse() {
    return new SaveToJsonJaxbResponse();
  }

  /**
   * Create an instance of {@link SaveToXmlJackson }
   *
   */
  public SaveToXmlJackson createSaveToXmlJackson() {
    return new SaveToXmlJackson();
  }

  /**
   * Create an instance of {@link SaveToXmlJacksonResponse }
   *
   */
  public SaveToXmlJacksonResponse createSaveToXmlJacksonResponse() {
    return new SaveToXmlJacksonResponse();
  }

  /**
   * Create an instance of {@link SaveToXmlJaxb }
   *
   */
  public SaveToXmlJaxb createSaveToXmlJaxb() {
    return new SaveToXmlJaxb();
  }

  /**
   * Create an instance of {@link SaveToXmlJaxbResponse }
   *
   */
  public SaveToXmlJaxbResponse createSaveToXmlJaxbResponse() {
    return new SaveToXmlJaxbResponse();
  }

  /**
   * Create an instance of {@link Session }
   *
   */
  public Session createSession() {
    return new Session();
  }

  /**
   * Create an instance of {@link SqlException }
   *
   */
  public SqlException createSqlException() {
    return new SqlException();
  }

  /**
   * Create an instance of {@link Exception }
   *
   */
  public Exception createException() {
    return new Exception();
  }

  /**
   * Create an instance of {@link Throwable }
   *
   */
  public Throwable createThrowable() {
    return new Throwable();
  }

  /**
   * Create an instance of {@link StackTraceElement }
   *
   */
  public StackTraceElement createStackTraceElement() {
    return new StackTraceElement();
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link AccessForbiddenException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link AccessForbiddenException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "AccessForbiddenException") public JAXBElement<AccessForbiddenException> createAccessForbiddenException(AccessForbiddenException value) {
    return new JAXBElement<AccessForbiddenException>(_AccessForbiddenException_QNAME, AccessForbiddenException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link ClassNotFoundException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "ClassNotFoundException") public JAXBElement<ClassNotFoundException> createClassNotFoundException(ClassNotFoundException value) {
    return new JAXBElement<ClassNotFoundException>(_ClassNotFoundException_QNAME, ClassNotFoundException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link CloneNotSupportedException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link CloneNotSupportedException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "CloneNotSupportedException") public JAXBElement<CloneNotSupportedException> createCloneNotSupportedException(CloneNotSupportedException value) {
    return new JAXBElement<CloneNotSupportedException>(_CloneNotSupportedException_QNAME, CloneNotSupportedException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link EmptyRepositoryException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link EmptyRepositoryException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "EmptyRepositoryException") public JAXBElement<EmptyRepositoryException> createEmptyRepositoryException(EmptyRepositoryException value) {
    return new JAXBElement<EmptyRepositoryException>(_EmptyRepositoryException_QNAME, EmptyRepositoryException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link EmptyStringException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link EmptyStringException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "EmptyStringException") public JAXBElement<EmptyStringException> createEmptyStringException(EmptyStringException value) {
    return new JAXBElement<EmptyStringException>(_EmptyStringException_QNAME, EmptyStringException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link IOException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "IOException") public JAXBElement<IOException> createIOException(IOException value) {
    return new JAXBElement<IOException>(_IOException_QNAME, IOException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "JAXBException") public JAXBElement<JAXBException> createJAXBException(JAXBException value) {
    return new JAXBElement<JAXBException>(_JAXBException_QNAME, JAXBException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SQLException }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "SQLException") public JAXBElement<SQLException> createSQLException(SQLException value) {
    return new JAXBElement<SQLException>(_SQLException_QNAME, SQLException.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBin }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromBin }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromBin") public JAXBElement<LoadFromBin> createLoadFromBin(LoadFromBin value) {
    return new JAXBElement<LoadFromBin>(_LoadFromBin_QNAME, LoadFromBin.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromBinResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromBinResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromBinResponse") public JAXBElement<LoadFromBinResponse> createLoadFromBinResponse(LoadFromBinResponse value) {
    return new JAXBElement<LoadFromBinResponse>(_LoadFromBinResponse_QNAME, LoadFromBinResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJackson }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJackson }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromJsonJackson") public JAXBElement<LoadFromJsonJackson> createLoadFromJsonJackson(LoadFromJsonJackson value) {
    return new JAXBElement<LoadFromJsonJackson>(_LoadFromJsonJackson_QNAME, LoadFromJsonJackson.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJacksonResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJacksonResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromJsonJacksonResponse") public JAXBElement<LoadFromJsonJacksonResponse> createLoadFromJsonJacksonResponse(LoadFromJsonJacksonResponse value) {
    return new JAXBElement<LoadFromJsonJacksonResponse>(_LoadFromJsonJacksonResponse_QNAME, LoadFromJsonJacksonResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJaxb }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJaxb }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromJsonJaxb") public JAXBElement<LoadFromJsonJaxb> createLoadFromJsonJaxb(LoadFromJsonJaxb value) {
    return new JAXBElement<LoadFromJsonJaxb>(_LoadFromJsonJaxb_QNAME, LoadFromJsonJaxb.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJaxbResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromJsonJaxbResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromJsonJaxbResponse") public JAXBElement<LoadFromJsonJaxbResponse> createLoadFromJsonJaxbResponse(LoadFromJsonJaxbResponse value) {
    return new JAXBElement<LoadFromJsonJaxbResponse>(_LoadFromJsonJaxbResponse_QNAME, LoadFromJsonJaxbResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJackson }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJackson }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromXmlJackson") public JAXBElement<LoadFromXmlJackson> createLoadFromXmlJackson(LoadFromXmlJackson value) {
    return new JAXBElement<LoadFromXmlJackson>(_LoadFromXmlJackson_QNAME, LoadFromXmlJackson.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJacksonResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJacksonResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromXmlJacksonResponse") public JAXBElement<LoadFromXmlJacksonResponse> createLoadFromXmlJacksonResponse(LoadFromXmlJacksonResponse value) {
    return new JAXBElement<LoadFromXmlJacksonResponse>(_LoadFromXmlJacksonResponse_QNAME, LoadFromXmlJacksonResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJaxb }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJaxb }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromXmlJaxb") public JAXBElement<LoadFromXmlJaxb> createLoadFromXmlJaxb(LoadFromXmlJaxb value) {
    return new JAXBElement<LoadFromXmlJaxb>(_LoadFromXmlJaxb_QNAME, LoadFromXmlJaxb.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJaxbResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromXmlJaxbResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "loadFromXmlJaxbResponse") public JAXBElement<LoadFromXmlJaxbResponse> createLoadFromXmlJaxbResponse(LoadFromXmlJaxbResponse value) {
    return new JAXBElement<LoadFromXmlJaxbResponse>(_LoadFromXmlJaxbResponse_QNAME, LoadFromXmlJaxbResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBin }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToBin }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToBin") public JAXBElement<SaveToBin> createSaveToBin(SaveToBin value) {
    return new JAXBElement<SaveToBin>(_SaveToBin_QNAME, SaveToBin.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToBinResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToBinResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToBinResponse") public JAXBElement<SaveToBinResponse> createSaveToBinResponse(SaveToBinResponse value) {
    return new JAXBElement<SaveToBinResponse>(_SaveToBinResponse_QNAME, SaveToBinResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonJackson }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToJsonJackson }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToJsonJackson") public JAXBElement<SaveToJsonJackson> createSaveToJsonJackson(SaveToJsonJackson value) {
    return new JAXBElement<SaveToJsonJackson>(_SaveToJsonJackson_QNAME, SaveToJsonJackson.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonJacksonResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToJsonJacksonResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToJsonJacksonResponse") public JAXBElement<SaveToJsonJacksonResponse> createSaveToJsonJacksonResponse(SaveToJsonJacksonResponse value) {
    return new JAXBElement<SaveToJsonJacksonResponse>(_SaveToJsonJacksonResponse_QNAME, SaveToJsonJacksonResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonJaxb }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToJsonJaxb }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToJsonJaxb") public JAXBElement<SaveToJsonJaxb> createSaveToJsonJaxb(SaveToJsonJaxb value) {
    return new JAXBElement<SaveToJsonJaxb>(_SaveToJsonJaxb_QNAME, SaveToJsonJaxb.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToJsonJaxbResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToJsonJaxbResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToJsonJaxbResponse") public JAXBElement<SaveToJsonJaxbResponse> createSaveToJsonJaxbResponse(SaveToJsonJaxbResponse value) {
    return new JAXBElement<SaveToJsonJaxbResponse>(_SaveToJsonJaxbResponse_QNAME, SaveToJsonJaxbResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlJackson }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToXmlJackson }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToXmlJackson") public JAXBElement<SaveToXmlJackson> createSaveToXmlJackson(SaveToXmlJackson value) {
    return new JAXBElement<SaveToXmlJackson>(_SaveToXmlJackson_QNAME, SaveToXmlJackson.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlJacksonResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToXmlJacksonResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToXmlJacksonResponse") public JAXBElement<SaveToXmlJacksonResponse> createSaveToXmlJacksonResponse(SaveToXmlJacksonResponse value) {
    return new JAXBElement<SaveToXmlJacksonResponse>(_SaveToXmlJacksonResponse_QNAME, SaveToXmlJacksonResponse.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlJaxb }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToXmlJaxb }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToXmlJaxb") public JAXBElement<SaveToXmlJaxb> createSaveToXmlJaxb(SaveToXmlJaxb value) {
    return new JAXBElement<SaveToXmlJaxb>(_SaveToXmlJaxb_QNAME, SaveToXmlJaxb.class, null, value);
  }

  /**
   * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlJaxbResponse }{@code >}
   *
   * @param value
   *     Java instance representing xml element's value.
   * @return
   *     the new instance of {@link JAXBElement }{@code <}{@link SaveToXmlJaxbResponse }{@code >}
   */
  @XmlElementDecl(namespace = "http://api.tm.zolov.ru/", name = "saveToXmlJaxbResponse") public JAXBElement<SaveToXmlJaxbResponse> createSaveToXmlJaxbResponse(SaveToXmlJaxbResponse value) {
    return new JAXBElement<SaveToXmlJaxbResponse>(_SaveToXmlJaxbResponse_QNAME, SaveToXmlJaxbResponse.class, null, value);
  }

}
